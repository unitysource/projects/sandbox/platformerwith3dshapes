﻿using UnityEngine;

namespace Core
{
    public static class Constants
    {
        public static Color GizmosColor = new Color(0.57f, 0f, 0.21f);
    }
}