﻿using System;
using UnityEngine;

namespace Core
{
    /// <summary>
    /// Draws the field/property ONLY if the copared property compared by the comparison type with the value of comparedValue returns true.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    public class DrawIfAttribute : PropertyAttribute
    {
        public string PropName { get; }
        public object Value { get; }

        /// <summary>
        /// Only draws the field only if a condition is met.
        /// </summary>
        /// <param name="propName">The name of the property that is being compared (case sensitive).</param>
        /// <param name="value">The value the property is being compared to.</param>
        public DrawIfAttribute(string propName, object value)
        {
            PropName = propName;
            Value = value;
        }
    }
}
