﻿using System;
using Mechanics.PlatformerPhysics;
using Mehanics;
using Mehanics.Enemy;
using Tools;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Infrastructures
{
    public class EnemyFactory : IEnemyFactory
    {
        private const string Idle = "Prefabs/Enemies/Idle";
        private const string IdleProjectile = "Prefabs/Enemies/IdleProjectile";
        private const string PlatformWalker = "Prefabs/Enemies/PlatformWalker";
        private const string PlatformAttentionWalker = "Prefabs/Enemies/PlatformAttentionWalker";
        private const string CleverWalker = "Prefabs/Enemies/CleverWalker";
        private const string RepeatWalker = "Prefabs/Enemies/RepeatWalker";
        private readonly DiContainer _diContainer;

        private Object _idle;
        private Object _idleProjectile;
        private Object _platformWalker;
        private Object _platformAttentionWalker;
        private Object _cleverWalker;
        private Object _repeatWalker;

        public EnemyFactory(DiContainer diContainer) =>
            _diContainer = diContainer;

        public void Load()
        {
            _idle = Resources.Load(Idle);
            _idleProjectile = Resources.Load(IdleProjectile);
            _platformWalker = Resources.Load(PlatformWalker);
            _platformAttentionWalker = Resources.Load(PlatformAttentionWalker);
            _cleverWalker = Resources.Load(CleverWalker);
            _repeatWalker = Resources.Load(RepeatWalker);
        }

        public void Create(EnemyType enemyType, Transform at)
        {
            switch (enemyType)
            {
                case EnemyType.Idle:
                    _diContainer.InstantiatePrefab(_idle, at.position, Quaternion.identity, null);
                    break;
                case EnemyType.IdleProjectile:
                    GameObject projInstance =
                        _diContainer.InstantiatePrefab(_idleProjectile, at.position, Quaternion.identity, null);
                    GameObject projectilePrefab = at.GetComponent<EnemyMarker>().projectilePrefab;
                    projInstance.GetComponent<ProjectileIdle>().Projectile = projectilePrefab;
                    break;
                case EnemyType.PlatformWalker:
                    GameObject instance =
                        _diContainer.InstantiatePrefab(_platformWalker, at.position, Quaternion.identity, null);
                    MoveDirection direction = at.GetComponent<EnemyMarker>().moveDirection;
                    instance.GetComponent<PlatformWalker>().Direction = direction;
                    break;
                case EnemyType.PlatformAttentionWalker:
                    _diContainer.InstantiatePrefab(_platformAttentionWalker, at.position, Quaternion.identity, null);
                    break;
                case EnemyType.CleverWalker:
                    _diContainer.InstantiatePrefab(_cleverWalker, at.position, Quaternion.identity, null);
                    break;
                case EnemyType.RepeatWalker:
                    _diContainer.InstantiatePrefab(_repeatWalker, at.position, Quaternion.identity, null);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(enemyType), enemyType, null);
            }
        }
    }
}