﻿using Mechanics.PlatformerPhysics;
using Mehanics;
using UnityEngine;

namespace Infrastructures
{
    public interface IEnemyFactory
    {
        void Load();
        void Create(EnemyType enemyType, Transform at);
    }
}