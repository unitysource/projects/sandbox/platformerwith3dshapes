﻿using Mehanics;
using UnityEngine;

namespace Infrastructures
{
    public interface IShapeFactory
    {
        void Load();
        GameObject Create(ShapeType shapeType, Vector2 at);
    }
}