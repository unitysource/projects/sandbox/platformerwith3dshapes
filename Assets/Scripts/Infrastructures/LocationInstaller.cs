﻿using Mechanics.PlatformerPhysics;
using Mehanics.Character;
using Tools;
using UnityEngine;
using Zenject;

namespace Infrastructures
{
    public class LocationInstaller : MonoInstaller
        // , IInitializable
    {
        [SerializeField] private Transform startPoint;

        [SerializeField] private GameObject playerPrefab;
        // [SerializeField] private EnemyMarker[] enemyMarkers;

        public override void InstallBindings()
        {
            BindInstallerInterfaces();
            BindInput();
            BindDirections();
            BindPlayer();
            BindEnemyFactory();
            BindShapeFactory();
        }

        private void BindInput()
        {
            var input = new Core.InputSystem();
            Container.Bind<Core.InputSystem>().FromInstance(input.With(_ => input.Enable())).AsSingle();
        }

        private void BindDirections()
        {
            var direction = startPoint.GetComponent<StartDirection>();
            Container.Bind<StartDirection>().FromInstance(direction).AsSingle();
        }

        private void BindEnemyFactory()
        {
            Container.Bind<IEnemyFactory>().To<EnemyFactory>().AsSingle();
        }

        private void BindShapeFactory()
        {
            Container.Bind<IShapeFactory>().To<ShapeFactory>().AsSingle();
        }

        private void BindInstallerInterfaces()
        {
            Container.BindInterfacesTo<LocationInstaller>().FromInstance(this).AsSingle();
        }

        private void BindPlayer()
        {
            var playerController = Container.InstantiatePrefabForComponent<CharacterController2D>(playerPrefab,
                startPoint.position, Quaternion.identity, null);
            Container.Bind<CharacterController2D>().FromInstance(playerController).AsSingle();
        }

        // public void Initialize()
        // {
        //     var enemyFactory = Container.Resolve<IEnemyFactory>();
        //     enemyFactory.Load();
        //     
        //     foreach (var marker in enemyMarkers) 
        //         enemyFactory.Create(marker.enemyType, marker.transform.position);
        // }
    }
}