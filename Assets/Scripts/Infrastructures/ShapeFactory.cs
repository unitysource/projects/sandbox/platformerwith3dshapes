﻿using System;
using Mechanics.PlatformerPhysics;
using Mehanics;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Infrastructures
{
    public class ShapeFactory : IShapeFactory
    {
        private const string CubeRotation = "Prefabs/Shapes/CubeRotation";
        private const string CubeScale = "Prefabs/Shapes/CubeScale";
        private const string SphereScale = "Prefabs/Shapes/SphereScale";
        private const string TriangleScale = "Prefabs/Shapes/TriangleScale";
        private readonly DiContainer _diContainer;

        private Object _cubeRotationShape;
        private Object _cubeScaleShape;
        private Object _sphereScaleShape;
        private Object _triangleScaleShape;

        public ShapeFactory(DiContainer diContainer) => 
            _diContainer = diContainer;

        public void Load()
        {
            _cubeRotationShape = Resources.Load(CubeRotation);
            _cubeScaleShape = Resources.Load(CubeScale);
            _sphereScaleShape = Resources.Load(SphereScale);
            _triangleScaleShape = Resources.Load(TriangleScale);
        }

        public GameObject Create(ShapeType shapeType, Vector2 at)
        {
            return shapeType switch
            {
                ShapeType.CubeRotate => _diContainer.InstantiatePrefab(_cubeRotationShape, at, Quaternion.identity, null),
                ShapeType.CubeScale => _diContainer.InstantiatePrefab(_cubeScaleShape, at, Quaternion.identity, null),
                ShapeType.SphereScale => _diContainer.InstantiatePrefab(_sphereScaleShape, at, Quaternion.identity, null),
                ShapeType.TriangleScale => _diContainer.InstantiatePrefab(_triangleScaleShape, at, Quaternion.identity, null),
                _ => throw new ArgumentOutOfRangeException(nameof(shapeType), shapeType, null)
            };
        }
        
        // public void Create(ShapeType shapeType, Vector2 at)
        // {
        //     switch (shapeType)
        //     {
        //         case ShapeType.CubeRotate:
        //             _diContainer.InstantiatePrefab(_cubeRotationShape, at, Quaternion.identity, null);
        //             break;
        //         case ShapeType.CubeScale:
        //             _diContainer.InstantiatePrefab(_cubeScaleShape, at, Quaternion.identity, null);
        //             break;
        //         case ShapeType.SphereScale:
        //             _diContainer.InstantiatePrefab(_sphereScaleShape, at, Quaternion.identity, null);
        //             break;
        //         case ShapeType.TriangleScale:
        //             _diContainer.InstantiatePrefab(_triangleScaleShape, at, Quaternion.identity, null);
        //             break;
        //         default:
        //             throw new ArgumentOutOfRangeException(nameof(shapeType), shapeType, null);
        //     }
        // }
    }
}