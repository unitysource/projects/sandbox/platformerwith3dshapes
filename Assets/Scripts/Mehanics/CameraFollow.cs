﻿using System;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics
{
    public class CameraFollow : MonoBehaviour
    {
        private Transform _player;
        private float _zOffset;
        [SerializeField] private float speed;

        [Inject]
        private void Construct(CharacterController2D playerController)
        {
            _player = playerController.transform;
            _zOffset = transform.position.z;
        }

        private void FixedUpdate()
        {
            // transform.position = Vector3.Lerp(transform.position, _player.position + Vector3.forward * _zOffset,
            //     speed * Time.deltaTime);
            
            transform.position = Vector3.Lerp(transform.position, new Vector3(_player.position.x, _player.position.y-2, _zOffset), 
                speed * Time.deltaTime);
        }
    }
}