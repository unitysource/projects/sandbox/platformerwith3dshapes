﻿using Mechanics.PlatformerPhysics;
using UnityEngine;

namespace Mehanics.Character
{
    public class CharacterController2D : PhysicsObject2D
    {
        [SerializeField] [Range(0, 15)] private float maxWalkingSpeed = 5;
        [SerializeField] [Range(0, 15)] private float maxRunningSpeed = 8;
        [SerializeField] [Range(0, 15)] private float jumpTakeOffSpeed = 5;

        private Vector2 _moveDirection;
        private bool _jump;
        private bool _move;
        private bool _run;


        protected override void OnVelocityCompute()
        {
            // reset all states
            if (!_move) _moveDirection.Set(0, 0);
            else _move = false;

            // select speed modifier between default and running 
            float speed = _run ? maxRunningSpeed : maxWalkingSpeed;
            // apply movement
            targetVelocity = _moveDirection * speed;
        }


        /// <summary>
        /// Move current character by direction delta
        /// using custom platformer physics 
        /// </summary>
        /// <param name="direction">Distance and direction to move</param>
        /// <param name="run">Use "running" mode</param>
        public void Move(float direction, bool run = false)
        {
            _moveDirection.x = direction;
            _move = true;
            _run = run;
        }

        /// <summary>
        /// Make one jump for current character by direction delta
        /// using custom platformer physics 
        /// </summary>
        public void Jump()
        {
            if (!grounded) return;
            velocity.y = jumpTakeOffSpeed;
        }

        public static void Revert() =>
            PlayerManager.Delta = PlayerManager.Delta.x >= 1 ? Vector2.left : Vector2.right;

        public void Death()
        {
            Debug.Log("player death");
            Destroy(gameObject, .3f);
        }
    }
}