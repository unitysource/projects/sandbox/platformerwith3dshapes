﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using Mechanics.PlatformerPhysics;
using Mehanics;
using Mehanics.Character;
using Tools;
using UnityEngine;
using Zenject;

namespace Character
{
    [RequireComponent(typeof(CharacterController2D))]
    public class CharacterInteract : MonoBehaviour
    {
        private CharacterController2D _characterController;
        private const float VerticalOffset = 0.7f;
        private const float HorizontalOffset = 0.1f;
        private readonly float[] _verticalOffsets = {-VerticalOffset, 0f, VerticalOffset};
        private readonly float[] _horizontalOffsets = {-HorizontalOffset, 0f, HorizontalOffset};
        private int _layerMask;


        [Inject]
        private void Construct(StartDirection startDirection)
        {
            _characterController = GetComponent<CharacterController2D>();
            _layerMask = LayerMask.GetMask("Wall");
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.JumpTrigger))
            {
                _characterController.Jump();
            }
            else if (other.CompareTag(Tags.Wall))
            {
                CharacterController2D.Revert();
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(Layers.Wall))
            {
                TryRevertingCharacter();
                TryCharacterJump();
            }
            else if (other.gameObject.CompareTag(Tags.Enemy))
            {
                _characterController.Death();
            }
        }

        private void TryRevertingCharacter()
        {
            if(HitValues(_verticalOffsets))
                CharacterController2D.Revert();
        }

        private void TryCharacterJump()
        {
            if (HitValues(_horizontalOffsets))
                StartCoroutine(JumpIe());
        }

        private IEnumerator JumpIe()
        {
            bool canJump = false;
            while (!canJump)
            {
                yield return null;
                canJump = !HitValues(_horizontalOffsets);
            }
            _characterController.Jump();
        }

        private bool HitValues(IReadOnlyList<float> offsets)
        {
            var position = transform.position;
            // Choose vertical or horizontal direction
            var direction = offsets == _horizontalOffsets ? Vector2.down :
                PlayerManager.Delta == Vector2.right ? Vector2.right : Vector2.left;

            const float laserLength = 2f;
            RaycastHit2D[] hits = new RaycastHit2D[offsets.Count];
            
            for (var i = 0; i < _horizontalOffsets.Length; i++)
            {
                Vector2 startPosition = new Vector2(
                    position.x + (offsets == _horizontalOffsets ? offsets[i] : 0),
                    position.y + (offsets == _verticalOffsets ? offsets[i] : 0));
                hits[i] = Physics2D.Raycast(startPosition, direction, laserLength, _layerMask);
                Debug.DrawRay(startPosition, direction * laserLength, Color.red, 2f);
            }

            return !hits.All(hit => hit.collider is null);
        }
    }
}