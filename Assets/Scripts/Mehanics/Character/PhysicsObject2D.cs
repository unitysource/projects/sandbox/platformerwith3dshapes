﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Mechanics.PlatformerPhysics
{
    /// <summary>
    /// Custom physics implementation specially for platformer games
    /// Learn more: https://learn.unity.com/tutorial/live-session-2d-platformer-character-controller#5c7f8528edbc2a002053b68f
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class PhysicsObject2D : MonoBehaviour
    {
        [SerializeField] protected float minGroundNormalY = 0.65f;
        [SerializeField] protected float gravityScale = 1f;

        protected bool grounded;
        protected new Rigidbody2D rigidbody;
        protected ContactFilter2D contactFilter;
        protected Vector2 groundNormal;
        protected Vector2 velocity;
        protected Vector2 targetVelocity;

        private Vector2 _groundNormalCache;
        private readonly RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
        private readonly List<RaycastHit2D> _hitBufferList = new List<RaycastHit2D>(16);

        private const float MIN_MOVE_DISTANCE = 0.001f;
        private const float SHELL_RADIUS = 0.01f;


        protected virtual void Start()
        {
            rigidbody = GetComponent<Rigidbody2D>();
            contactFilter.useTriggers = false;
            // Get Unity collision filters for current layer
            contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            contactFilter.useLayerMask = true;
        }

        protected virtual void Update()
        {
            targetVelocity = Vector2.zero;
            // Override to customize velocity calculation
            // For example, use player inputs
            OnVelocityCompute();
        }

        protected virtual void OnVelocityCompute()
        {
        }

        protected virtual void FixedUpdate()
        {
            // Gravity
            velocity += Physics2D.gravity * (gravityScale * Time.deltaTime);

            velocity.x = targetVelocity.x;
            grounded = false;

            var deltaPosition = velocity * Time.deltaTime;
            _groundNormalCache.Set(groundNormal.y, -groundNormal.x);
            var direction = _groundNormalCache * deltaPosition.x;

            ApplyMovement(direction, false);
            direction = Vector2.up * deltaPosition.y;
            ApplyMovement(direction, true);
        }

        private void ApplyMovement(Vector2 direction, bool yMovement)
        {
            float distance = direction.magnitude;

            if (distance > MIN_MOVE_DISTANCE)
            {
                int count = rigidbody.Cast(direction, contactFilter, _hitBuffer, distance + SHELL_RADIUS);
                _hitBufferList.Clear();
                for (int i = 0; i < count; i++) _hitBufferList.Add(_hitBuffer[i]);

                foreach (var raycastHit in _hitBufferList)
                {
                    var currentNormal = raycastHit.normal;
                    if (currentNormal.y > minGroundNormalY)
                    {
                        grounded = true;
                        if (yMovement)
                        {
                            groundNormal = currentNormal;
                            currentNormal.x = 0;
                        }
                    }

                    float projection = Vector2.Dot(velocity, currentNormal);
                    if (projection < 0) velocity -= projection * currentNormal;

                    float modifiedDistance = raycastHit.distance - SHELL_RADIUS;
                    distance = modifiedDistance < distance ? modifiedDistance : distance;
                }
            }

            rigidbody.position += direction.normalized * distance;
        }


#if UNITY_EDITOR
        private void Reset()
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.simulated = true;
            rb.useFullKinematicContacts = true;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
#endif
    }
}