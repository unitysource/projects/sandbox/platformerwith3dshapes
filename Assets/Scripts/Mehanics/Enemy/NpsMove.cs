﻿using Mechanics.PlatformerPhysics;
using Mehanics.Character;
using UnityEngine;
using Zenject;

// [RequireComponent(typeof(Rigidbody2D))]
namespace Mehanics
{
    public class NpsMove : MonoBehaviour
    {
        private Transform _player;

        [SerializeField] private float speed = 1f;
        // private Rigidbody2D _rb;


        [Inject]
        private void Construct(CharacterController2D playerController)
        {
            // _rb = GetComponent<Rigidbody2D>();
            _player = playerController.transform;
        }

        private void Update()
        {
            if(_player == null) return;
            transform.position = Vector3.MoveTowards(transform.position,
                _player.position, speed * Time.deltaTime);
        }
    }
}