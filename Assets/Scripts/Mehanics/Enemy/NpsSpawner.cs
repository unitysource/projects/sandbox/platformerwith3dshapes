﻿using Core;
using Infrastructures;
using Mechanics.PlatformerPhysics;
using UnityEngine;
using Zenject;

namespace Mehanics
{
    public class NpsSpawner : MonoBehaviour
    {
        private EnemyMarker[] _enemyMarkers;

        [Inject]
        private void Construct(IEnemyFactory enemyFactory)
        {
            enemyFactory.Load();
            var enemyMarkerObjects = GameObject.FindGameObjectsWithTag(Tags.EnemyMarker);
            _enemyMarkers = new EnemyMarker[enemyMarkerObjects.Length];
            for (var i = 0; i < enemyMarkerObjects.Length; i++)
                _enemyMarkers[i] = enemyMarkerObjects[i].GetComponent<EnemyMarker>();
            foreach (var marker in _enemyMarkers) 
                enemyFactory.Create(marker.enemyType, marker.transform);
        }
    }
}