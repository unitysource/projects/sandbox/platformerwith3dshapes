﻿using Mechanics.PlatformerPhysics;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics.Enemy
{
    public class PlatformAttentionWalker : PlatformWalker
    {
        [SerializeField] private float attentionDistance = 5f;
        private Transform _target;

        [Inject]
        private void Construct(CharacterController2D playerController)
        {
            _target = playerController.transform;
        }

        public override void FixedUpdate()
        {
            float distance = Vector2.Distance(transform.position, _target.position);
            if (distance <= attentionDistance && Hit.collider && CanFollow)
            {
                if (CurrTransform.position.x <= _target.position.x && Direction is MoveDirection.Left ||
                    CurrTransform.position.x > _target.position.x && Direction is MoveDirection.Right)
                {
                    Direction = MoveDirectionExtensions.ChangeDirection(Direction);
                    RevertScale();
                }
            }
            else if (distance > attentionDistance && Hit.collider)
                CanFollow = true;

            base.FixedUpdate();
        }
    }
}