﻿using Tools;
using UnityEngine;

namespace Mehanics.Enemy
{
    public class PlatformWalker : MonoBehaviour
    {
        [SerializeField] protected float speed = 2f;

        private Transform _groundChecker;

        public MoveDirection Direction { get; set; }
        private LayerMask _groundMask;
        protected RaycastHit2D Hit;
        protected Transform CurrTransform;
        private Vector3 _scale = new Vector3();

        protected bool CanFollow = true;

        [HideInInspector] public Rigidbody2D rb;

        private void Start()
        {
            _groundChecker = transform.GetChild(0);
            rb = GetComponent<Rigidbody2D>();
            _groundMask = LayerMask.GetMask(Layers.Ground);
            CurrTransform = transform;

            if (Direction is MoveDirection.Right) RevertScale();

            Hit = Physics2D.Raycast(_groundChecker.position, -CurrTransform.up, 2f, _groundMask);
        }

        private void Update() =>
            Hit = Physics2D.Raycast(_groundChecker.position, -transform.up, 2f, _groundMask);

        public virtual void FixedUpdate()
        {
            var dir = Direction is MoveDirection.Right ? 1 : -1;
            if (Hit.collider)
                rb.velocity = new Vector2(speed * dir, rb.velocity.y);
            else
            {
                Direction = MoveDirectionExtensions.ChangeDirection(Direction);
                RevertScale();
                CanFollow = false;
            }
        }
        
        protected void RevertScale()
        {
            var scale = CurrTransform.localScale;
            _scale.Set(scale.x, scale.y, scale.z);
            CurrTransform.localScale = new Vector3(-scale.x, scale.y, scale.z);
        }
    }
}