﻿using System.Collections;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics.Enemy
{
    public class ProjectileIdle : MonoBehaviour
    {
        public GameObject Projectile { get; set; }

        [SerializeField] private float shotDelay = 2f;
        [SerializeField] private float force = 15f;
        [SerializeField] private float attentionDistance = 15f;

        private Vector2 _projectileStartPosition;
        private const float ProjectileStartPositionOffset = 1f;
        private Transform _target;

        [Inject]
        private void Construct(CharacterController2D playerController)
        {
            _target = playerController.transform;

            var position = transform.position;
            _projectileStartPosition = new Vector2(position.x, position.y + ProjectileStartPositionOffset);
        }


        private IEnumerator Start()
        {
            while (true)
            {
                yield return new WaitForSeconds(shotDelay);
                
                float distance = Vector2.Distance(transform.position, _target.position);
                if (distance > attentionDistance) yield break;
                
                GameObject newProjectile = Instantiate(Projectile, _projectileStartPosition, Quaternion.identity);
                newProjectile.TryGetComponent<Rigidbody2D>(out var selfRigidbody);

                if (selfRigidbody)
                {
                    selfRigidbody.AddForce(new Vector2((_target.position.x - transform.position.x)*0.4f, force), ForceMode2D.Impulse);
                }
                else
                {
                    Debug.LogWarning($"projectile {Projectile.name} doesn't have Rigidbody2D component");
                }
            }
        }
    }
}