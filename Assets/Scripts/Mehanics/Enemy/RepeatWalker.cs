﻿using System;
using Mechanics.PlatformerPhysics;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics.Enemy
{
    public class RepeatWalker : MonoBehaviour
    {
        [SerializeField] private float attentionDistance = 5f;
        private Transform _target;
        private Vector3 _distance;

        [Inject]
        private void Construct(CharacterController2D playerController)
        {
            _target = playerController.transform;
            _distance = _target.position - transform.position;
        }
        
        private void FixedUpdate()
        {
            float distance = Vector2.Distance(transform.position, _target.position);
            if (distance <= attentionDistance)
            {
                transform.position = _target.position + _distance;
            }
            else
            {
            }
        }
    }
}