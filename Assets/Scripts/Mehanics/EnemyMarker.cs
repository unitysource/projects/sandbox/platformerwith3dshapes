﻿using Core;
using Mechanics.PlatformerPhysics;
using UnityEngine;

namespace Mehanics
{
    public class EnemyMarker : MonoBehaviour
    {
        public EnemyType enemyType;

        [Space(10)]
        [DrawIf("enemyType", EnemyType.PlatformWalker)]
        public MoveDirection moveDirection = MoveDirection.Right;
        
        [DrawIf("enemyType", EnemyType.IdleProjectile)]
        public GameObject projectilePrefab;

        private void OnDrawGizmos()
        {
            var position = transform.position;
            Gizmos.color = Constants.GizmosColor;
            Gizmos.DrawWireSphere(position, 1);
            Gizmos.color = Color.white;
        }
    }
}