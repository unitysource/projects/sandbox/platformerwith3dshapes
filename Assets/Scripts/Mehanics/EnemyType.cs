﻿using System;

namespace Mehanics
{
    // [Flags]
    public enum EnemyType
    {
        Idle,
        IdleProjectile,
        PlatformWalker,
        PlatformAttentionWalker,
        CleverWalker,
        RepeatWalker
    }
}