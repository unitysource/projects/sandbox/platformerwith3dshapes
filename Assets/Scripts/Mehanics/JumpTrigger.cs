﻿using System;
using Core;
using UnityEngine;

namespace Mehanics
{
    public class JumpTrigger : MonoBehaviour
    {
        [SerializeField] private bool isDisposable = true;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.s_Player) && isDisposable)
            {
                Destroy(gameObject, 0.01f);
            }
        }
    }
}