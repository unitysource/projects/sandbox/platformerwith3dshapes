﻿namespace Mehanics
{
    public enum MoveDirection
    {
        Left,
        Right
    }

    public static class MoveDirectionExtensions
    {
        public static MoveDirection ChangeDirection(MoveDirection currentDirection) =>
            currentDirection is MoveDirection.Right ? MoveDirection.Left : MoveDirection.Right;

    }
}