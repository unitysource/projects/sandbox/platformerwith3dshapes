﻿using Mechanics.PlatformerPhysics;
using Mehanics.Character;
using UnityEngine;
using Zenject;

namespace Mehanics
{
    [RequireComponent(typeof(CharacterController2D))]
    public class PlayerManager : MonoBehaviour
    {
        private InputSystem _inputSystem;
        private CharacterController2D _characterController;

        public static Vector2 Delta { get; set; }

        [Inject]
        private void Construct(StartDirection startDirection)
        {
            _characterController = GetComponent<CharacterController2D>();
        
            Delta = startDirection.StartDirection1 is MoveDirection.Left ? Vector2.left : Vector2.right;
        }
    
        private void Start()
        {
            _inputSystem = new InputSystem();
            _inputSystem.Enable();

            // _inputSystem.Player.Move.performed += context => OnMove(context.ReadValue<Vector2>());
        }

        private void Update()
        {
            _characterController.Move(Delta.x);
        }
    }
}