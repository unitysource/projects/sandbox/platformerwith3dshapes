﻿using System;
using System.Collections;
using Tools;
using UnityEngine;

namespace Mehanics
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float lifeTime = 2f;
        
        // [SerializeField] private ParticleSystem deathParticles;

        private void Start()
        {
            if(gameObject)
                Destroy(gameObject, lifeTime);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer(Layers.Ground))
            {
                Destroy(gameObject, .01f);
            }
        }
    }
}