﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Mehanics;
using UnityEngine;
using UnityEngine.UI;

namespace Mechanics.PlatformerPhysics
{
    public class ChangeShapes : MonoBehaviour
    {
        [SerializeField] private Slider changeSlider;
        private readonly List<Transform> _scaleShapes = new List<Transform>(3);
        private readonly List<Transform> _rotateYShapes = new List<Transform>(3);

        private void Start()
        {
            changeSlider.value = changeSlider.maxValue/2;
            InitShapes();
            
            InstantiateShape.OnPointerUpEvent += InitShapes;
        }

        private void InitShapes()
        {
            AddTransforms(Tags.ScaleShape, _scaleShapes);
            AddTransforms(Tags.RotationYShape, _rotateYShapes);
            OnChangeScale();
            OnChangeRotationY();
        }

        public void OnChangeScale()
        {
            var valueNormalized = ValueNormalization(2);
            foreach (var scaleShape in _scaleShapes)
                scaleShape.localScale = Vector3.one * valueNormalized + Vector3.one;
        }

        public void OnChangeRotationY()
        {
            const float maxValue = 80f;
            var valueNormalized = ValueNormalization(maxValue);
            foreach (var shape in _rotateYShapes)
                shape.rotation = Quaternion.Euler(Vector3.up * (maxValue - valueNormalized));
        }
        
        private static void AddTransforms(string objTag, List<Transform> shapesList)
        {
            List<GameObject> shapes = GameObject.FindGameObjectsWithTag(objTag).ToList();
            shapesList.AddRange(shapes.Select(shapeObj => shapeObj.transform));
        }

        private float ValueNormalization(float maxValue) =>
            changeSlider.value * maxValue;
    }
}