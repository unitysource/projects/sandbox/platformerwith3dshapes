﻿using System;
using Infrastructures;
using Tools;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Mehanics
{
    public class InstantiateShape : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private Transform _child;
        private ShapeType _childShape;
        private GameObject _currentShape;
        private SpriteRenderer _curRenderer;
        private Color _defaultColor;
        private const float MinDistance = 5.5f;
        private bool _canCreate = true;
        private IShapeFactory _shapeFactory;


        private int _size;
        private LayerMask[] _detectMasks;
        private int _detect;

        public static bool CanMove = true;
        public static event Action OnPointerUpEvent;

        private Color CollisionDefaultColor =>
            new Color(_defaultColor.r + 0.2f, _defaultColor.g, _defaultColor.b, 0.4f);


        [Inject]
        private void Construct(IShapeFactory shapeFactory)
        {
            _shapeFactory = shapeFactory;
            _shapeFactory.Load();
            _child = transform.GetChild(0);
            _childShape = GetComponentInChildren<Shape>().ShapeType;

            _detectMasks = new LayerMask[]
            {
                LayerMask.GetMask(Layers.Ground),
                LayerMask.GetMask(Layers.Enemy),
                LayerMask.GetMask(Layers.Wall),
                LayerMask.GetMask(Layers.Player),
            };

            _detect = _detectMasks[0] | _detectMasks[1] | _detectMasks[2] | _detectMasks[3];
        }

        private void Update()
        {
            if (_currentShape is null) return;

            _size = Physics2D.OverlapCircleNonAlloc(_currentShape.transform.position, 1.8f,
                new Collider2D[3],
                _detect);
            _curRenderer.color = _size > 0 ? CollisionDefaultColor : _defaultColor;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!_canCreate) return;
            if (CanMove is false) CanMove = true;
            InstantiationShape();
            Time.timeScale = 0;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Time.timeScale = 1;
            CanMove = false;

            if (_currentShape is null) return;

            float radius = _currentShape.transform.localScale.x * 0.5f + 0.3f;

            var position = _currentShape.transform.position;
            Utils.DrawEllipse(position, _currentShape.transform.forward, _currentShape.transform.up, radius,
                radius, 32, Color.black, 3f);

            if (NotSupportDistance() || _size > 0)
                Destroy(_currentShape, 0.02f);
            else
            {
                _child.gameObject.SetActive(false);
                _currentShape.layer = LayerMask.NameToLayer(Layers.Wall);
                _curRenderer.sortingOrder = 0;
                _curRenderer.color = _defaultColor;
                OnPointerUpEvent?.Invoke();
                _canCreate = false;
            }

            _currentShape = null;

            bool NotSupportDistance() =>
                Vector2.Distance(_currentShape.transform.position, _child.position) < MinDistance;
        }

        private void InstantiationShape()
        {
            _currentShape = _shapeFactory.Create(_childShape, _child.position);
            _curRenderer = _currentShape.GetComponent<SpriteRenderer>();
            _curRenderer.sortingOrder = 3;
            _defaultColor = _curRenderer.color;
            if (_currentShape.layer != LayerMask.NameToLayer(Layers.Selected))
                _currentShape.layer = LayerMask.NameToLayer(Layers.Selected);
        }
    }
}