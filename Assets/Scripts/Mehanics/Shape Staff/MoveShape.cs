﻿using Tools;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using static UnityEngine.InputSystem.InputAction;

namespace Mehanics.Shape_Staff
{
    public class MoveShape : MonoBehaviour
    {
        private Camera _camera;
        private Core.InputSystem _input;

        private bool _held;


        [Inject]
        private void Construct(Core.InputSystem inputSystem)
        {
            _input = inputSystem;
            _input.Shape.Move.started += OnMoveStarted;
            _input.Shape.Move.canceled += OnMoveCanceled;
        }

        private void OnDestroy()
        {
            _input.Shape.Move.started -= OnMoveStarted;
            _input.Shape.Move.canceled -= OnMoveCanceled;
        }

        private void Start() => _camera = Camera.main;

        private void Update()
        {
            Vector2 mousePos = _camera.ScreenToWorldPoint(Mouse.current.position.ReadValue());

            if (!_held || !InstantiateShape.CanMove) return;
            
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero, default,
                LayerMask.GetMask(Layers.Selected));
            if (!hit)
                return;

            hit.transform.position = mousePos;    
        }

        private void OnMoveStarted(CallbackContext ctx) => _held = true;

        private void OnMoveCanceled(CallbackContext ctx) => _held = false;
    }
}