﻿using UnityEngine;

namespace Mehanics
{
    public class Shape : MonoBehaviour
    {
        [SerializeField] private ShapeType shapeType;

        public ShapeType ShapeType => shapeType;
    }
}