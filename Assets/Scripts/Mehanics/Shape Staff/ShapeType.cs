﻿namespace Mehanics
{
    public enum ShapeType
    {
        SphereScale,
        CubeRotate,
        CubeScale,
        TriangleScale
    }
}