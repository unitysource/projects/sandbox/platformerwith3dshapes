﻿using Mehanics;
using UnityEngine;

namespace Mechanics.PlatformerPhysics
{
    public class StartDirection : MonoBehaviour
    {
        [SerializeField] private MoveDirection startDirection;

        public MoveDirection StartDirection1 => startDirection;
    }
}