﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Mehanics
{
    public class TimeChanger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            Time.timeScale = 0;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Time.timeScale = 1;
        }
    }
}