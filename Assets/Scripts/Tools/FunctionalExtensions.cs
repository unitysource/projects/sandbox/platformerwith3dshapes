﻿using System;
using UnityEngine;

namespace Tools
{
    public static class FunctionalExtensions
    {
        public static T With<T>(this T self, Action<T> set)
        {
            set.Invoke(self);
            return self;
        }
        
        public static T With<T>(this T self, Action<T> apply, Func<bool> when)
        {
            if(when())
                apply?.Invoke(self);
            
            return self;
        }
        
        public static T With<T>(this T self, Action<T> apply, bool when)
        {
            if(when)
                apply?.Invoke(self);
            
            return self;
        }

        // example
        private static GameObject SetupItem(GameObject itemToStore, string name, bool isKinematic = false) =>
            itemToStore
                .With(x => x.name = name)
                .With(x => x.GetComponent<Rigidbody2D>().isKinematic = isKinematic)
                .With(x => x.GetComponent<Rigidbody2D>().isKinematic = true, when: isKinematic);
    }
}