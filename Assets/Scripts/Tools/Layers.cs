﻿namespace Tools
{
    public static class Layers
    {
        public const string Wall = "Wall";
        public const string Enemy = "Enemy";
        public const string Player = "Player";
        public const string Ground = "Ground";
        public const string Selected = "Selected";
    }
}