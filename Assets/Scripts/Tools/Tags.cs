﻿﻿namespace Core
{
    public static class Tags
    {
        public const string s_Player = "Player";
        public const string Enemy = "Enemy";
        public const string s_Ground = "Ground";
        public const string s_Finish = "Finish";
        public const string s_Rope = "Rope";
        public const string s_Core = "Core";
        public const string s_Bullet = "Bullet";
        public const string s_Gravity = "Gravity";
        public const string s_Teleport = "Teleport";
        public const string s_Block = "Block";
        public const string s_Mill = "Mill";
        public const string Star = "Star";
        public const string JumpTrigger = "JumpTrigger";
        public const string ScaleShape = "ScaleShape";
        public const string RotationYShape = "RotationYShape";
        public const string EnemyMarker = "EnemyMarker";
        public const string Wall = "Wall";
    }
}